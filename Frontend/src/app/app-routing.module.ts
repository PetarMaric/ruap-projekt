import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ActivationComponent } from './components/activation/activation.component';
import { AccountRecoveryComponent } from './components/account-recovery/account-recovery.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { PredictionComponent } from './components/prediction/prediction.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ApplicationGuardService } from './common/services/application.guard.service';

const navigationRoutes: Routes = [
    //{ path:'', redirectTo:"/register", pathMatch:"full" },
    { path: '', redirectTo: "/register", pathMatch: "full" },
    { path: 'home', component: HomeComponent, canActivate: [ApplicationGuardService] },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'activation', component: ActivationComponent },
    { path: 'accountRecovery', component: AccountRecoveryComponent },
    { path: 'passwordReset', component: PasswordResetComponent },
    { path: 'prediction', component: PredictionComponent, canActivate: [ApplicationGuardService] },
    { path: 'notfound', component: PageNotFoundComponent },
    { path: '**', component: PageNotFoundComponent }
];



@NgModule({
    imports: [
        RouterModule.forRoot(navigationRoutes)],
    exports: [RouterModule],
    declarations: []
})
export class AppRoutingModule {

}
