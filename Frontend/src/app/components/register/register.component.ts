import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../../common/models/register-model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../common/services/authentication.service';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ViewChild } from '@angular/core';
import { AlertService } from '../../common/services/alert.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    registerDetails: RegisterModel;

    registerForm: FormGroup;
    username: FormControl;
    email: FormControl;
    passwords: FormGroup;
    password: FormControl;
    repeatedPassword: FormControl;
    recaptchaControl: FormControl;


    // siteKey dobiven registracijom localhost na admin panelu reCaptche
    siteKey: string = "6Le0K2kUAAAAABRRofEYk-P-nKBK9OFd_3hIQVdw";
    // ViewChild obavezan dekorator da se prepozna  recaptcha komponenta unutar templatea
    @ViewChild(ReCaptchaComponent) recaptcha: ReCaptchaComponent;
    recaptchaToken: string;

    constructor(private authenticationService: AuthenticationService,
        private router: Router,
        private alertService: AlertService) { }

    ngOnInit() {

        this.registerDetails = new RegisterModel();

        this.createFormControls();
        this.createForm();

    }

    createFormControls() {

        this.username = new FormControl('', Validators.required);
        //this.email = new FormControl('', [Validators.required, Validators.email]);
        this.email = new FormControl('', [Validators.required, Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$")]);
        this.password = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern("^(?=.*[a-z,A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-.,-/]).{8,50}$")]);
        this.repeatedPassword = new FormControl('', [Validators.required]);
        this.recaptchaControl = new FormControl('', [Validators.required]);

        this.passwords = new FormGroup({
            password: this.password,
            repeatedPassword: this.repeatedPassword
        }, { validators: this.matchValidator });
    }

    createForm() {

        this.registerForm = new FormGroup({
            username: this.username,
            email: this.email,
            passwords: this.passwords,
            recaptchaControl: this.recaptchaControl
        });

    }

    // https://stackoverflow.com/a/35475248/4937897
    matchValidator(group: FormGroup) {
        var valid = false;

        if (group.controls['password'].value != undefined && group.controls['repeatedPassword'].value != undefined) {
            let password: string = group.controls['password'].value
            let repeatedPassword: string = group.controls['repeatedPassword'].value

            /* If user first enters 'password' and then starts to type 'repeatedPassword', the 'valid' flag
             * will be set to true only if 'repeatedPassword' is equal to 'password' and "repeatedPassword.match(password)"
             * does this check. But if user first enters 'password', then 'repeatedPassword' and then changes 'password', the 
             * password check will pass because of this expression: "repeatedPassword.match(password)." In order to check both 'password'
             * and 'repeatedPassword' for equality, second expression is added: "password.match(repeatedPassword)."
             * */
            valid = (repeatedPassword.match(password) && password.match(repeatedPassword)) ? true : false;

            if (valid) {
                return null;
            }

        }

        return {
            mismatch: true
        };

    }

    handleCorrectCaptcha(event) {
        this.registerDetails.recaptchaToken = this.recaptchaToken = this.recaptcha.getResponse().toString();
    }

    register() {
        if (this.registerForm.valid == false) {
            this.alertService.error("Form has errors. Please correct them.");
        }
        else {
            this.authenticationService.register(this.registerDetails).then(
                success => this.successfulRegistration(success),
                error => this.unsuccessfulRegistration(error, this.registerDetails)
            );
        }

    }

    successfulRegistration(response: object) {
        this.alertService.success("Registration is successful. Please check your email address in order to confirm your created account.")
    }

    unsuccessfulRegistration(response: object, registerDetails: RegisterModel) {
        if (response["statusText"] === "Conflict") {
            if (response["data"] === "DuplicateUserName") {
                this.alertService.error("Username " + registerDetails.username + " is already taken. Please enter new username and try again.");
            }
            if (response["data"] === "DuplicateEmail") {
                this.alertService.error("Email " + registerDetails.email + " is already taken. Please enter new email and try again.");
            }
        }
        else {
            this.alertService.error("There was an unexpected error.");
        }

    }

}
