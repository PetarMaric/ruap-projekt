import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../common/services/alert.service';

import { Alert } from '../../common/models/alert';
import { AlertType } from '../../common/models/alert-type';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
    alerts: Alert[] = [];

    constructor(private alertService: AlertService) {

    }

    ngOnInit() {
        this.alertService.getAlert().subscribe((message: Alert) => {
            if (!message) {
                // https://stackoverflow.com/a/29803364
                this.alerts.length = 0;
            }

            this.alerts.push(message);
        });
    }

    removeAlert(message: Alert) {
        this.alerts = this.alerts.filter(x => x !== message);
        console.log(this.alerts);
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-error';
        }
    }


}
