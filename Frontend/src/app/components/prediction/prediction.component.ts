import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CensusIncome } from '../../common/models/census-income-data-set-values';
import { CensusIncomeEntries } from '../../common/models/census-income-data-set-entries'
import { FormControl, Form, FormGroup, Validators, NgForm, ValidatorFn, AbstractControl } from '@angular/forms';
import { PredictionService } from '../../common/services/prediction.service';
import { IExperimentResponse } from 'src/app/common/models/IExperimentResponse';
import { ExperimentData } from '../../common/models/experiment-data';
import { isRegExp } from 'util';



@Component({
    selector: 'app-prediction',
    templateUrl: './prediction.component.html',
    styleUrls: ['./prediction.component.css']
})


export class PredictionComponent implements OnInit {

    machineLearningObject: CensusIncomeEntries;


    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private predictionService: PredictionService) { }


    education: string[];
    workclass: string[];
    maritalStatus: string[];
    occupation: string[];
    relationship: string[];
    race: string[];
    sex: string[];
    nativeCountry: string[];

    age: number[];
    hoursPerWeek: number[];
    educationNum: number[];
    fnlwgt: number;
    capitalGain: number;
    capitalLoss: number;


    educationEntry: string;


    predictionForm: FormGroup;


    educationForm: FormControl;
    workclassForm: FormControl;
    maritalStatusForm: FormControl;
    occupationForm: FormControl;
    relationshipForm: FormControl;
    raceForm: FormControl;
    sexForm: FormControl;
    nativeCountryForm: FormControl;
    ageForm: FormControl;
    fnlwgtForm: FormControl;
    educationNumForm: FormControl;
    capitalGainForm: FormControl;
    capitalLossForm: FormControl;
    hoursPerWeekForm: FormControl;


    experimentData: ExperimentData;

    experimentDataResponse: IExperimentResponse;



    ngOnInit() {
        this.machineLearningObject = new CensusIncomeEntries();

        this.education = CensusIncome.EDUCATION;
        this.workclass = CensusIncome.WORKCLASS;
        this.maritalStatus = CensusIncome.MARITAL_STATUS;
        this.occupation = CensusIncome.OCCUPATION;
        this.relationship = CensusIncome.RELATIONSHIP;
        this.race = CensusIncome.RACE;
        this.sex = CensusIncome.SEX;
        this.nativeCountry = CensusIncome.NATIVE_COUNTRY;
        this.age = CensusIncome.AGE;
        this.hoursPerWeek = CensusIncome.HOURS_PER_WEEK;
        this.educationNum = CensusIncome.EDUCATION_NUM;

        this.createFormControls();
        this.createForm();

    }

    createFormControls() {
        this.educationForm = new FormControl(null, Validators.required);
        this.workclassForm = new FormControl(null, Validators.required);
        this.maritalStatusForm = new FormControl(null, Validators.required);
        this.occupationForm = new FormControl(null, Validators.required);
        this.relationshipForm = new FormControl(null, Validators.required);
        this.raceForm = new FormControl(null, Validators.required);
        this.sexForm = new FormControl(null, Validators.required);
        this.nativeCountryForm = new FormControl(null, Validators.required);
        this.ageForm = new FormControl(null, Validators.required);
        // Validators.max - 100_000_000_000
        this.fnlwgtForm = new FormControl(null, [Validators.required, Validators.max(100000000000), Validators.min(0), Validators.pattern("^([1-9][0-9]{0,11}|0)$")]);
        this.educationNumForm = new FormControl(null, Validators.required);
        this.capitalGainForm = new FormControl(null, [Validators.required, Validators.max(100000000000), Validators.min(0), Validators.pattern("^([1-9][0-9]{0,11}|0)$")]);
        this.capitalLossForm = new FormControl(null, [Validators.required, Validators.max(100000000000), Validators.min(0), Validators.pattern("^([1-9][0-9]{0,11}|0)$")]);
        this.hoursPerWeekForm = new FormControl(null, Validators.required);

    }


    createForm() {
        this.predictionForm = new FormGroup({
            education: this.educationForm,
            age: this.ageForm,
            workclass: this.workclassForm,
            fnlwgt: this.fnlwgtForm,
            educationNum: this.educationNumForm,
            maritalStatus: this.maritalStatusForm,
            occupation: this.occupationForm,
            relationship: this.relationshipForm,
            race: this.raceForm,
            sex: this.sexForm,
            capitalGain: this.capitalGainForm,
            capitalLoss: this.capitalLossForm,
            hoursPerWeek: this.hoursPerWeekForm,
            nativeCountry: this.nativeCountryForm
        });
    }

    makeExperimentCall(): void {
        this.experimentData = new ExperimentData();
        this.experimentData.map(this.machineLearningObject);

        //console.log("Mapiran objekt forme u eksperiment tip:\n" + JSON.stringify(this.experimentData, null, '  '));

        this.predictionService.makeExperiment(this.experimentData).then((success) => {
            let data = success as IExperimentResponse;
            this.experimentDataResponse = data;
            console.log("Prediction value: " + data.predictionValue);
            console.log("Prediction probability : " + data.probability);
            console.log("Prediction post date: " + data.postDate);

            let element: HTMLElement = document.getElementById('modalActivationButton') as HTMLElement;
            element.click();
        },
            (error) => {
                let element: HTMLElement = document.getElementById('modalErrorButton') as HTMLElement;
                element.click();
            });

    }

}
