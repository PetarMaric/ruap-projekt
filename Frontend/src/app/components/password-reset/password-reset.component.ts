import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../common/services/authentication.service';
import { AlertService } from '../../common/services/alert.service';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordResetModel } from '../../common/models/password-reset-model';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

    passwordResetDetail: PasswordResetModel;

    passwordResetForm: FormGroup;
    password: FormControl;
    repeatedPassword: FormControl;
    passwords: FormGroup;

    passwordRecoveryToken: string = "";


    siteKey: string = "6Le0K2kUAAAAABRRofEYk-P-nKBK9OFd_3hIQVdw";
    recaptchaToken: string;
    @ViewChild(ReCaptchaComponent) recaptcha: ReCaptchaComponent;


    constructor(private authenticationService: AuthenticationService,
        private router: Router,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute) { }






    ngOnInit() {
        this.passwordResetDetail = new PasswordResetModel();


        this.createFormControl();
        this.createForm();


        this.activatedRoute.queryParams.subscribe(params => {
            this.passwordRecoveryToken = params["passwordRecoveryToken"];
            localStorage.setItem("passwordRecoveryToken", this.passwordRecoveryToken);
        });
    }

    createFormControl() {

        this.password = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern("^(?=.*[a-z,A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-.,-/]).{8,50}$")]);
        this.repeatedPassword = new FormControl('', [Validators.required]);


        this.passwords = new FormGroup({
            password: this.password,
            repeatedPassword: this.repeatedPassword
        }, { validators: this.matchValidator });
    }

    createForm() {

        this.passwordResetForm = new FormGroup({
            passwords: this.passwords
        });

    }

    matchValidator(group: FormGroup) {
        var valid = false;

        if (group.controls['password'].value != undefined && group.controls['repeatedPassword'].value != undefined) {
            let password: string = group.controls['password'].value
            let repeatedPassword: string = group.controls['repeatedPassword'].value

            valid = (repeatedPassword.match(password) && password.match(repeatedPassword)) ? true : false;

            if (valid) {
                return null;
            }

        }

        return {
            mismatch: true
        };

    }

    changePassword() {
        if (this.passwordResetForm.valid == false) {
            this.alertService.error("Passwords must match.");
        }
        else {
            this.authenticationService.passwordRenewal(this.passwordResetDetail).then(
                response => {
                    this.alertService.success("Your password has been changed. Shortly, you will be redirected to login screen.");
                    setTimeout((router: Router) => {
                        this.router.navigateByUrl("login");
                    }, 1000);
                },
                error => {
                    this.alertService.error("There was a problem with your request.");
                });

        }

    }
}
