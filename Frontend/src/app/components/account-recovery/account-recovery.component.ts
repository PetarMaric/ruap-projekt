import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../common/services/authentication.service';
import { Router } from '@angular/router';
import { AlertService } from '../../common/services/alert.service';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountRecoveryModel } from '../../common/models/account-recovery-model';


@Component({
    selector: 'app-account-recovery',
    templateUrl: './account-recovery.component.html',
    styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {

    accountRecoveryDetails: AccountRecoveryModel;

    accountRecoveryForm: FormGroup;
    email: FormControl;
    recaptchaControl: FormControl;

    siteKey: string = "6Le0K2kUAAAAABRRofEYk-P-nKBK9OFd_3hIQVdw";
    recaptchaToken: string;
    @ViewChild(ReCaptchaComponent) recaptcha: ReCaptchaComponent;

    constructor(private authenticationService: AuthenticationService,
        private router: Router,
        private alertService: AlertService) { }

    ngOnInit() {
        this.accountRecoveryDetails = new AccountRecoveryModel();

        this.createFormControls();
        this.createForm();
    }

    createFormControls() {
        this.email = new FormControl('', [Validators.required, Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$")])
        this.recaptchaControl = new FormControl('', [Validators.required]);
    }

    createForm() {
        this.accountRecoveryForm = new FormGroup({
            email: this.email,
            recaptchaControl: this.recaptchaControl
        });
    }

    handleCorrectCaptcha(event) {
        this.accountRecoveryDetails.recaptchaToken = this.recaptcha.getResponse().toString();
    }

    resetPassword() {
        if (this.accountRecoveryForm.valid == false) {
            this.alertService.error("Email and recaptcha puzzle have to be valid.");
        }
        else {
            this.authenticationService.resetPassword(this.accountRecoveryDetails).then(response => {
                this.alertService.success("Email has been sent to your inbox. It will contain necessary details for password reset procedure.");
            },
                error => {
                    this.alertService.error("There was a problem with your request.");
                });
        }

    }

}
