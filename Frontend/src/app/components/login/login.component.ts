import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../common/services/authentication.service';
import { LoginModel } from '../../common/models/login-model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from '../../common/services/alert.service';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ViewChild } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';




@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginDetails: LoginModel;

    loginForm: FormGroup;
    email: FormControl;
    password: FormControl;

    recaptchaControl: FormControl;

    loginError: string;

    loggedInToken: string;


    loginErrorBoolean: boolean = false;
    submit: boolean = false;



    siteKey: string = "6Le0K2kUAAAAABRRofEYk-P-nKBK9OFd_3hIQVdw";
    recaptchaToken: string;
    @ViewChild(ReCaptchaComponent) recaptcha: ReCaptchaComponent;

    waitingForResponse: boolean = false;


    constructor(private authenticationService: AuthenticationService,
        private router: Router,
        private alertService: AlertService) { }

    ngOnInit() {
        //this.authenticationService.logout();

        this.createFormControls();
        this.createForm();

        this.loginDetails = new LoginModel();
    }

    createFormControls() {
        // Validators.pattern("[^ @]*@[^ @]*");
        // Validators.pattern("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$")     // https://www.regular-expressions.info/email.html

        //this.email = new FormControl('', [Validators.required, Validators.pattern("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}$")]);
        this.email = new FormControl('', [Validators.required, Validators.email]);
        this.password = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern("^(?=.*[a-z,A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-.,-/]).{8,50}$")]);
    }

    createForm() {
        this.loginForm = new FormGroup({
            email: this.email,
            password: this.password
        });
    }


    login() {
        this.submit = true;
        this.authenticationService.login(this.loginDetails.email, this.loginDetails.password).then(
            response => {
                this.loggedInToken = response;
                setTimeout(() => this.router.navigateByUrl('prediction'), 1000);
                this.alertService.success("Successfuly logged in. You will be redirected shortly.")
                //this.router.navigateByUrl('dashboard')
            },
            error => {
                this.loginError = "Error while logging in the Baasic. Please try again.";
                this.alertService.error("Your email or password is incorrect");
                this.loginErrorBoolean = true;

            });
    }

}
