import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthenticationService } from '../../common/services/authentication.service';


@Component({
    selector: 'app-activation',
    templateUrl: './activation.component.html',
    styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {
    activationToken: string = "";

    constructor(private authenticationService: AuthenticationService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.activationToken = params["activationToken"];
            localStorage.setItem("activationToken", this.activationToken);
        });
    }

    activateAccount() {
        this.authenticationService.activate();
    }


}

