import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BaasicApp } from 'baasic-sdk-angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AlertComponent } from './components/alert/alert.component';
import { AppRoutingModule } from './app-routing.module';
import { AlertService } from './common/services/alert.service';
import { LoginComponent } from './components/login/login.component';
import { AuthenticationService } from './common/services/authentication.service';
import { HttpModule } from '@angular/http';
import { TokenService } from './common/services/token.service';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { ActivationComponent } from './components/activation/activation.component';
import { AccountRecoveryComponent } from './components/account-recovery/account-recovery.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { PredictionComponent } from '../app/components/prediction/prediction.component';
import { HttpClientModule } from '@angular/common/http';
import { PredictionService } from './common/services/prediction.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ApplicationGuardService } from './common/services/application.guard.service';


@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        LoginComponent,
        HomeComponent,
        RegisterComponent,
        ActivationComponent,
        AccountRecoveryComponent,
        PasswordResetComponent,
        PredictionComponent,
        PageNotFoundComponent

    ],
    imports: [
        BrowserModule,
        BaasicApp.forRoot('mlnet-elasticsearch-angular', {
            apiRootUrl: 'api.baasic.com',
            apiVersion: 'v1'
        }),
        AppRoutingModule,
        ReactiveFormsModule,
        HttpModule,
        ReCaptchaModule,
        FormsModule,
        HttpClientModule

    ],
    providers: [AlertService, AuthenticationService, TokenService, PredictionService, ApplicationGuardService],
    bootstrap: [AppComponent]
})
export class AppModule {


}
