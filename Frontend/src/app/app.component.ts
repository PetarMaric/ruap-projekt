import { Component } from '@angular/core';
import { NavigationStart, NavigationEnd, NavigationError, NavigationCancel, Router, Event } from '@angular/router';
import { AuthenticationService } from './common/services/authentication.service';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'ML.NET frontend';

    showIndicator: boolean = true;


    constructor(private _router: Router,
        private _authenticationService: AuthenticationService) {

        this._router.events.subscribe((routerEvent: Event) => {
            if (routerEvent instanceof NavigationStart) {
                this.showIndicator = true;
            }

            if (routerEvent instanceof NavigationEnd) {
                this.showIndicator = false;
            }

            if (routerEvent instanceof NavigationEnd
                || routerEvent instanceof NavigationError
                || routerEvent instanceof NavigationCancel) {
                this.showIndicator = false;
            }
        });
    }

    logout() {
        this._authenticationService.logout();
    }


}
