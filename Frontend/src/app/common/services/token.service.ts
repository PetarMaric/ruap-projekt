import { Injectable } from '@angular/core';
import { BaasicAppService, MembershipService } from 'baasic-sdk-angular';
import { IToken } from 'baasic-sdk-javascript';

@Injectable()
export class TokenService {

    constructor(
        private baasicAppService: BaasicAppService
    ) { }

    getToken(): IToken {
        return this.baasicAppService.getAccessToken();
    }

    removeToken(): void {
        this.baasicAppService.updateAccessToken(null);
    }

    isExpired(): boolean {
        let token = this.getToken();
        if (token) {
            return token.expireTime < (new Date()).getTime();
        }

        return true;
    }
}
