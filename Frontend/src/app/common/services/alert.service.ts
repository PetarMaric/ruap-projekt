import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';

import { Alert } from '../models/alert';
import { AlertType } from '../models/alert-type';

@Injectable()
export class AlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;

    // http://jasonwatmore.com/post/2017/06/25/angular-2-4-alert-toaster-notifications
    constructor(private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    this.keepAfterNavigationChange = false;
                }
                else {
                    this.clear();
                }
            }
        });
    }

    // "send alert"
    alert(type: AlertType, message: string, keepAfterRouteChange = false) {
        this.keepAfterNavigationChange = keepAfterRouteChange;
        this.subject.next(<Alert>{ type: type, message: message });
    }

    clear() {
        this.subject.next();
    }

    error(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Error, message, keepAfterRouteChange);
    }

    success(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Success, message, keepAfterRouteChange);
    }

    getAlert(): Observable<any> {
        return this.subject.asObservable();
    }







}
