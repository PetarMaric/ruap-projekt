import { TestBed, inject } from '@angular/core/testing';

import { ApplicationGuardService } from './application.guard.service';

describe('ApplicationGuardService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ApplicationGuardService]
        });
    });

    it('should be created', inject([ApplicationGuardService], (service: ApplicationGuardService) => {
        expect(service).toBeTruthy();
    }));
});
