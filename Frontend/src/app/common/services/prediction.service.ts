import { Injectable } from '@angular/core';
import { CensusIncomeEntries } from '../models/census-income-data-set-entries'
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import { IExperimentResponse } from '../models/IExperimentResponse';
import { ExperimentData } from '../models/experiment-data';

/*
@Injectable({
  providedIn: 'root'
})
*/
@Injectable()
export class PredictionService {

    //constructor(private _httpClient: HttpModule) { }
    constructor(private _httpClient: HttpClient) { }

    makeExperiment(postData: ExperimentData): Promise<IExperimentResponse> {
        return this._httpClient.post<IExperimentResponse>("http://localhost:60396/api/predict", postData, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        }).toPromise();
    }




}
