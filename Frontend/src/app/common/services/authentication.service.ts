import { Injectable } from '@angular/core';

import { BaasicAppService, MembershipService } from 'baasic-sdk-angular';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { RegisterModel } from '../models/register-model';
import { AccountRecoveryModel } from '../models/account-recovery-model';
import { PasswordResetModel } from '../models/password-reset-model';
import { AlertService } from './alert.service';
import { TokenType } from '@angular/compiler';

@Injectable()
export class AuthenticationService {

    constructor(
        private baasicAppService: BaasicAppService,
        private membershipService: MembershipService,
        private tokenService: TokenService,
        private router: Router,
        private alertService: AlertService

    ) { }

    async login(username: string, password: string): Promise<string> {
        let data = {
            username: username,
            password: password,
            options: ['sliding']
        };

        let token = await this.membershipService.login.login(data);
        return token;
    }

    async logout(): Promise<void> {
        let token = this.tokenService.getToken();
        if (token) {
            this.membershipService.login.logout(token.token, token.type);
            this.tokenService.removeToken();
        }
    }

    async register(registerDetails: RegisterModel): Promise<object> {
        let response = await this.membershipService.register.create({
            displayName: registerDetails.username,
            activationUrl: "http://localhost:4200/activation?activationToken={activationToken}",
            challengeIdentifier: "mlnet-elasticsearch-angular",
            challengeResponse: registerDetails.recaptchaToken,
            confirmPassword: registerDetails.repeatedPassword,
            email: registerDetails.email,
            password: registerDetails.password,
            username: registerDetails.username
        });

        return response;
    }

    async activate(): Promise<void> {
        await this.membershipService.register.activate(localStorage.getItem("activationToken")).then(
            response => {
                this.alertService.success("You have successfully activated your account. Shortly, you will be redirected to login screen.");
                setTimeout((router: Router) => {
                    this.router.navigateByUrl("login");
                }, 1000);
                localStorage.removeItem("activationToken");
            },
            error => {
                if (error["statusCode"] === 404 && error["statusText"] === "Not Found") {
                    this.alertService.error("User account was already activated. You can close this window.");
                    localStorage.removeItem("activationToken");
                }
                else {
                    this.alertService.error("There was an error processing request. Your account has not been activated. Please try again later.");
                    localStorage.removeItem("activationToken");
                }
            });
    }

    async resetPassword(recoveryDetails: AccountRecoveryModel): Promise<object> {
        let response = await this.membershipService.passwordRecovery.requestReset({
            challengeIdentifier: "mlnet-elasticsearch-angular",
            challengeResponse: recoveryDetails.recaptchaToken,
            userName: recoveryDetails.email,
            recoverUrl: "http://localhost:4200/passwordReset?passwordRecoveryToken={passwordRecoveryToken}"
        });

        return response;

    }

    async passwordRenewal(passwordsModel: PasswordResetModel): Promise<object> {
        let response = await this.membershipService.passwordRecovery.reset({
            newPassword: passwordsModel.password,
            passwordRecoveryToken: localStorage.getItem("passwordRecoveryToken")
        });
        localStorage.removeItem("passwordRecoveryToken");
        return response;
    }

    checkLogin(): boolean {
        let token = this.tokenService.getToken();
        if (token) {
            return true;
        }
        else {
            return false;
        }

    }


}
