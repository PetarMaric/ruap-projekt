import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class ApplicationGuardService implements CanActivate {

    constructor(private _router: Router,
        private _authenticationService: AuthenticationService) { }


    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let loginStatus = this._authenticationService.checkLogin();
        if (loginStatus) {
            return true;
        }
        else {
            this._router.navigate(['notfound']);
            return false;
        }
    }

}
