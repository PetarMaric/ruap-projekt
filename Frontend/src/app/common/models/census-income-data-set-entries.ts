export class CensusIncomeEntries {

    private _age: number;
    private _workclass: string;
    private _fnlwgt: number;
    private _education: string;
    private _educationNum: number;
    private _maritalStatus: string;
    private _occupation: string;
    private _relationship: string;
    private _race: string;
    private _sex: string;
    private _capitalGain: number;
    private _capitalLoss: number;
    private _hoursPerWeek: number;
    private _nativeCountry: string;

    public constructor() { };

    public get age(): number {
        return this._age;
    }

    public set age(value: number) {
        this._age = value;
    }

    public get workclass(): string {
        return this._workclass;
    }

    public set workclass(value: string) {
        this._workclass = value;
    }

    public get fnlwgt(): number {
        return this._fnlwgt;
    }

    public set fnlwgt(value: number) {
        this._fnlwgt = value;
    }

    public get education(): string {
        return this._education;
    }

    public set education(value: string) {
        this._education = value;
    }

    public get educationNum(): number {
        return this._educationNum;
    }

    public set educationNum(value: number) {
        this._educationNum = value;
    }

    public get maritalStatus(): string {
        return this._maritalStatus;
    }

    public set maritalStatus(value: string) {
        this._maritalStatus = value;
    }

    public get occupation(): string {
        return this._occupation;
    }

    public set occupation(value: string) {
        this._occupation = value;
    }

    public get relationship(): string {
        return this._relationship;
    }

    public set relationship(value: string) {
        this._relationship = value;
    }

    public get race(): string {
        return this._race;
    }

    public set race(value: string) {
        this._race = value;
    }

    public get sex(): string {
        return this._sex;
    }

    public set sex(value: string) {
        this._sex = value;
    }

    public get capitalGain(): number {
        return this._capitalGain;
    }

    public set capitalGain(value: number) {
        this._capitalGain = value;
    }

    public get capitalLoss(): number {
        return this._capitalLoss;
    }

    public set capitalLoss(value: number) {
        this._capitalLoss = value;
    }

    public get hoursPerWeek(): number {
        return this._hoursPerWeek;
    }

    public set hoursPerWeek(value: number) {
        this._hoursPerWeek = value;
    }

    public get nativeCountry(): string {
        return this._nativeCountry;
    }

    public set nativeCountry(value: string) {
        this._nativeCountry = value
    }

}