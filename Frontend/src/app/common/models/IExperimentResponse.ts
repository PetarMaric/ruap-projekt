export interface IExperimentResponse {
    age: number;
    workclass: string;
    fnlwgt: number;
    education: string;
    educationNum: number;
    maritalStatus: string;
    occupation: string;
    relationship: string;
    race: string;
    sex: string;
    capitalGain: number;
    capitalLoss: number;
    hoursPerWeek: number;
    nativeCountry: string;

    id: number;
    postDate: Date;
    predictionValue: boolean;
    probability: number;
}
