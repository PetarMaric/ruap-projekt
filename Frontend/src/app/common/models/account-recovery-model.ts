export class AccountRecoveryModel {
    private _email: string;
    private _recaptchaToken: string;

    public constructor() { }

    public get email(): string {
        return this._email;
    }

    public set email(value: string) {
        this._email = value;
    }

    public get recaptchaToken(): string {
        return this._recaptchaToken;
    }

    public set recaptchaToken(value: string) {
        this._recaptchaToken = value;
    }

}