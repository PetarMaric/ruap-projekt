import { CensusIncomeEntries } from './census-income-data-set-entries';

export class ExperimentData {
    public age: number;
    public workclass: string;
    public fnlwgt: number;
    public education: string;
    public educationNum: number;
    public maritalStatus: string;
    public occupation: string;
    public relationship: string;
    public race: string;
    public sex: string;
    public capitalGain: number;
    public capitalLoss: number;
    public hoursPerWeek: number;
    public nativeCountry: string;

    public map(formData: CensusIncomeEntries): void {
        this.age = formData.age;
        this.workclass = formData.workclass;
        this.fnlwgt = formData.fnlwgt;
        this.education = formData.education;
        this.educationNum = formData.educationNum;
        this.maritalStatus = formData.maritalStatus;
        this.occupation = formData.occupation;
        this.relationship = formData.relationship;
        this.race = formData.race;
        this.sex = formData.sex;
        this.capitalGain = formData.capitalGain;
        this.capitalLoss = formData.capitalLoss;
        this.hoursPerWeek = formData.hoursPerWeek;
        this.nativeCountry = formData.nativeCountry;
    }
}