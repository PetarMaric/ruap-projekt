export class PasswordResetModel {
    private _password: string
    private _repeatedPassword: string

    public constructor() { }

    public get password(): string {
        return this._password;
    }

    public set password(value: string) {
        this._password = value;
    }

    public get repeatedPassword(): string {
        return this._repeatedPassword;
    }

    public set repeatedPassword(value: string) {
        this._repeatedPassword = value;
    }
}