
export class CensusIncome {
    public static readonly EDUCATION: string[] = [
        "10th",
        "11th",
        "12th",
        "1st-4th",
        "5th-6th",
        "7th-8th",
        "9th",
        "Assoc-acdm",
        "Assoc-voc",
        "Bachelors",
        "Doctorate",
        "HS-grad",
        "Masters",
        "Preschool",
        "Prof-school",
        "Some-college"];

    public static readonly MARITAL_STATUS: string[] = [
        "Divorced",
        "Married-AF-spouse",
        "Married-civ-spouse",
        "Married-spouse-absent",
        "Never-married",
        "Separated",
        "Widowed"];

    public static readonly OCCUPATION: string[] = [
        "Adm-clerical",
        "Armed-Forces",
        "Craft-repair",
        "Exec-managerial",
        "Farming-fishing",
        "Handlers-cleaners",
        "Machine-op-inspct",
        "Other-service",
        "Priv-house-serv",
        "Prof-specialty",
        "Protective-serv",
        "Sales",
        "Tech-support",
        "Transport-moving"];

    public static readonly RELATIONSHIP: string[] = ["Wife",
        "Husband",
        "Not-in-family",
        "Other-relative",
        "Own-child",
        "Unmarried"];

    public static readonly RACE: string[] = [
        "Amer-Indian-Eskimo",
        "Asian-Pac-Islander",
        "Black",
        "Other",
        "White"];

    public static readonly SEX: string[] = [
        "Female",
        "Male"];

    public static readonly NATIVE_COUNTRY: string[] = [
        "Cambodia",
        "Canada",
        "China",
        "Columbia",
        "Cuba",
        "Dominican-Republic",
        "Ecuador",
        "El-Salvador",
        "England",
        "France",
        "Germany",
        "Greece",
        "Guatemala",
        "Haiti",
        "Holand-Netherlands",
        "Honduras",
        "Hong",
        "Hungary",
        "India",
        "Iran",
        "Ireland",
        "Italy",
        "Jamaica",
        "Japan",
        "Laos",
        "Mexico",
        "Nicaragua",
        "Outlying-US(Guam-USVI-etc)",
        "Peru",
        "Philippines",
        "Poland",
        "Portugal",
        "Puerto-Rico",
        "Scotland",
        "South",
        "Taiwan",
        "Thailand",
        "Trinadad&Tobago",
        "Vietnam",
        "Yugoslavia",
        "United-States"];


    public static readonly WORKCLASS: string[] = [
        "Federal-gov",
        "Local-gov",
        "Never-worked",
        "Private",
        "Self-emp-inc",
        "Self-emp-not-inc",
        "State-gov",
        "Without-pay"];

    public static readonly AGE: number[] = [17,     18,     19,     20,     21,     22,     23,     24,     25,     26,     27,     28,     29,     30,     31,             
                                            32,     33,     34,     35,     36,     37,     38,     39,     40,     41,     42,     43,     44,     45,             
                                            46,     47,     48,     49,     50,     51,     52,     53,     54,     55,     56,     57,     58,     59,             
                                            60,     61,     62,     63,     64,     65,     66,     67,     68,     69,     70,     71,     72,     73,             
                                            74,     75,     76,     77,     78,     79,     80,     81,     82,     83,     84,     85,     86,     87,             
                                            88,     89,     90,     91,     92,     93,     94,     95,     96,     97,     98,     99];

    public static readonly HOURS_PER_WEEK: number[] = [  1,      2,      3,      4,      5,      6,      7,      8,      9,      10,     11,     12,     13,     14,
                                                        15,     16,     17,     18,     19,     20,     21,     22,     23,     24,     25,     26,     27,     28,             
                                                        29,     30,     31,     32,     33,     34,     35,     36,     37,     38,     39,     40,     41,     42,            
                                                        43,     44,     45,     46,     47,     48,     49,     50,     51,     52,     53,     54,     55,     56,             
                                                        57,     58,     59,     60,     61,     62,     63,     64,     65,     66,     67,     68,     69,     70,             
                                                        71,     72,     73,     74,     75,     76,     77,     78,     79,     80,     81,     82,     83,     84,             
                                                        85,     86,     87,     88,     89,     90,     91,     92,     93,     94,     95,     96,     97,     98,             
                                                        99,     100,    101,    102,    103,    104,    105,    106,    107,    108,    109,    110,    111,    112,            
                                                        113,    114,    115,    116,    117,    118,    119,    120,    121,    122,    123,    124,    125,    126,            
                                                        127,    128,    129,    130,    131,    132,    133,    134,    135,    136,    137,    138,    139,    140,            
                                                        141,    142,    143,    144,    145,    146,    147,    148,    149,    150,    151,    152,    153,    154,            
                                                        155,    156,    157,    158,    159,    160,    161,    162,    163,    164,    165,    166,    167,    168];


    public static readonly EDUCATION_NUM: number[] = [ 1,      2,      3,      4,      5,      6,      7,      8,      9,      10,     11,     12,     13,     14,
                                                      15,     16,     17,     18,     19,     20,     21,     22,     23,     24,     25,     26,     27,     28,             
                                                      29,     30,     31,     32,     33,     34,     35,     36,     37,     38,     39,     40,     41,     42,            
                                                      43,     44,     45,     46,     47,     48,     49,     50];



}
