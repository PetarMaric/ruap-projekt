"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var baasic_sdk_angular_1 = require("baasic-sdk-angular");
/*
@Injectable({
  providedIn: 'root'
})
*/
var TokenService = /** @class */ (function () {
    function TokenService(baasicAppService) {
        this.baasicAppService = baasicAppService;
    }
    TokenService.prototype.getToken = function () {
        return this.baasicAppService.getAccessToken();
    };
    TokenService.prototype.removeToken = function () {
        this.baasicAppService.updateAccessToken(null);
    };
    TokenService.prototype.isExpired = function () {
        var token = this.getToken();
        if (token) {
            return token.expireTime < (new Date()).getTime();
        }
        return true;
    };
    TokenService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [baasic_sdk_angular_1.BaasicAppService])
    ], TokenService);
    return TokenService;
}());
exports.TokenService = TokenService;
//# sourceMappingURL=token.service.js.map