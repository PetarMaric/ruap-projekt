"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("../../../../node_modules/rxjs/Subject");
var router_1 = require("@angular/router");
var AlertService = /** @class */ (function () {
    // http://jasonwatmore.com/post/2017/06/25/angular-2-4-alert-toaster-notifications
    function AlertService(_router) {
        var _this = this;
        this._router = _router;
        this._subject = new Subject_1.Subject();
        this._keepAfterNavigationChange = false;
        _router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (_this._keepAfterNavigationChange) {
                    _this._keepAfterNavigationChange = false;
                }
                else {
                    _this.clear();
                }
            }
        });
    }
    AlertService.prototype.success = function (successMessage, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this._keepAfterNavigationChange = keepAfterNavigationChange;
        this._subject.next({ type: 'success', text: successMessage });
    };
    AlertService.prototype.error = function (errorMessage, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this._keepAfterNavigationChange = keepAfterNavigationChange;
        this._subject.next({ type: 'error', text: errorMessage });
    };
    AlertService.prototype.warning = function (warningMessage, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this._keepAfterNavigationChange = keepAfterNavigationChange;
        this._subject.next({ type: 'warning', text: warningMessage });
    };
    AlertService.prototype.getStatusMessage = function () {
        return this._subject.asObservable();
    };
    AlertService.prototype.clear = function () {
        return this._subject.next();
    };
    AlertService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], AlertService);
    return AlertService;
}());
exports.AlertService = AlertService;
//# sourceMappingURL=alert.service.js.map