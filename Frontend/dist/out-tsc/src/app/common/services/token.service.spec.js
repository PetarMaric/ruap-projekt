"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var token_service_1 = require("./token.service");
describe('TokenService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [token_service_1.TokenService]
        });
    });
    it('should be created', testing_1.inject([token_service_1.TokenService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=token.service.spec.js.map