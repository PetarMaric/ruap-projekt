﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestApi.Utility;
using Classification;
using ElasticsearchSettings;
using ElasticsearchSettings.Classes;
using Microsoft.AspNetCore.Cors;

namespace RestApi.Controllers
{
    [ApiController]
    public class PredictController : ControllerBase
    {
        [HttpPost]
        [Route("api/{action = predict}")]
        public ActionResult<Prediction> Predict([FromBody] ClassificationData jsonFromBody)
        {

            var prediction = Classification.Program.MakePrediction(jsonFromBody);

            List<Prediction> predictionList = new List<Prediction>();
            predictionList.Add(prediction);

            Settings.StorePredictions(predictionList);


            return prediction;

        }
    }
}