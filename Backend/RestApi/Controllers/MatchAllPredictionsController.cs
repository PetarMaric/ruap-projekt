﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ElasticsearchSettings.Classes;
using ElasticsearchSettings;
using RestApi.Utility;

namespace RestApi.Controllers
{
    [ApiController]
    public class MatchAllPredictionsController : ControllerBase
    {
        [HttpGet]
        [Route("api/{action = matchall}")]
        public ActionResult<JObject> GetMatchAll(int size = Constraints.MATCH_ALL_DEFAULT)
        {

            int sizeLocal = size;

            List<Prediction> result = new List<Prediction>();

            if (size <= Constraints.MATCH_ALL_MAX && size >= Constraints.MATCH_ALL_MIN)
            {
                result = Settings.AcquireAllPredicions(sizeLocal);
            }
            else
            {
                return new JObject( new JProperty(JsonConstants.RECEIVED_QUERY, "Match all"),
                                    new JProperty(JsonConstants.WARNING, "Can not execute the query. Please choose the size between " + Constraints.MATCH_ALL_MAX + " and " + Constraints.MATCH_ALL_MIN + "."));
            }

            JObject json = new JObject(
                                            new JProperty(JsonConstants.QUERY, "Match all"),
                                            new JProperty(JsonConstants.SELECTED_SIZE, sizeLocal),
                                            new JProperty(JsonConstants.RESULT_COUNT, result.Count),
                                            new JProperty(JsonConstants.RESULTS, new JArray(
                                                                                      from p in result
                                                                                      select new JObject(
                                                                                                          new JProperty("Age", p.Age),
                                                                                                          new JProperty("Workclass", p.Workclass),
                                                                                                          new JProperty("Fnlwgt", p.Fnlwgt),
                                                                                                          new JProperty("Education", p.Education),
                                                                                                          new JProperty("EducationNum", p.EducationNum),
                                                                                                          new JProperty("MaritalStatus", p.MaritalStatus),
                                                                                                          new JProperty("Occupation", p.Occupation),
                                                                                                          new JProperty("Relationship", p.Relationship),
                                                                                                          new JProperty("Race", p.Race),
                                                                                                          new JProperty("Sex", p.Sex),
                                                                                                          new JProperty("CapitalGain", p.CapitalGain),
                                                                                                          new JProperty("CapitalLoss", p.CapitalLoss),
                                                                                                          new JProperty("NativeCountry", p.NativeCountry),
                                                                                                          new JProperty("HoursPerWeek", p.HoursPerWeek),
                                                                                                          new JProperty("PredictedClass", p.PredictionValue),
                                                                                                          new JProperty("PredictionProbability", p.Probability),
                                                                                                          new JProperty("PostDate", p.PostDate)))));

            return json;
        }
    }
}