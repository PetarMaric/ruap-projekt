﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Utility
{
    public class Constraints
    {
        /*      MATCH ALL CONSTRAINTS       */
        public const int MATCH_ALL_DEFAULT = 30;
        public const int MATCH_ALL_MIN = 1;
        public const int MATCH_ALL_MAX = 100;
    }
}
