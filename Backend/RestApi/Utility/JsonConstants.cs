﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Utility
{
    public class JsonConstants
    {
        public const string RECEIVED_QUERY = "ReceivedQuery";
        public const string WARNING = "Warning";
        public const string QUERY = "Query";
        public const string RESULT_COUNT = "ResultCount";
        public const string RESULTS = "Results";
        public const string SELECTED_SIZE = "SelectedSize";
    }
}
