﻿using System;
using System.Collections.Generic;
using System.Text;
using Nest;


namespace ElasticsearchSettings.Classes
{
    [ElasticsearchType(IdProperty = "Id", Name = "post")]
    public class Dataset
    {
        [IgnoreAttribute]
        public string Id
        {
            get
            {
                return Utility.CreateMD5(Age + Workclass + Fnlwgt + Education + EducationNum + MaritalStatus + Occupation
                    + Relationship + Race + Sex + CapitalGain + CapitalLoss + HoursPerWeek + NativeCountry + Income);
            }
        }
        public DateTime PostDate { get; set; }
        public float Age { get; set; }

        public string Workclass { get; set; }

        public float Fnlwgt { get; set; }

        public string Education { get; set; }


        public float EducationNum { get; set; }


        public string MaritalStatus { get; set; }


        public string Occupation { get; set; }

        public string Relationship { get; set; }


        public string Race { get; set; }

        public string Sex { get; set; }

        public float CapitalGain { get; set; }

        public float CapitalLoss { get; set; }


        public float HoursPerWeek { get; set; }

        public string NativeCountry { get; set; }
        public float Income { get; set; }
    }
}
