﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticsearchSettings
{
    public class Constants
    {
        /*      ELASTICSEARCH       */
        public const string ELASTIC_URI = "http://localhost:9200/";
        public const string ELASTIC_DATASET_INDEX_NAME = "dataset";
        public const string ELASTIC_PREDICTION_INDEX_NAME = "predictions";


    }
}
