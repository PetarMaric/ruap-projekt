﻿using System;
using System.Configuration;
using Nest;
using ElasticsearchSettings.Classes;
using System.Collections.Generic;

namespace ElasticsearchSettings
{

    public class Settings
    {
        private static ElasticClient _client = null;

        static Settings()
        {
            Uri node = new Uri(Constants.ELASTIC_URI);

            ConnectionSettings settings = new ConnectionSettings(node)
                .DisableDirectStreaming()
                .PrettyJson();

            _client = new ElasticClient(settings);

        }

        public static bool DeleteIndex(string indexName)
        {
            try
            {
                if (_client != null && _client.IndexExists(indexName).Exists == true)
                {
                    _client.DeleteIndexAsync(indexName);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CreateIndex<T>(string indexName) where T : class
        {
            try
            {
                if (_client != null && _client.IndexExists(indexName).Exists == false)
                {
                    _client.CreateIndex(indexName, c => c
                                .Settings(s => s
                                        .NumberOfShards(1)
                                        .NumberOfReplicas(0))
                                .Mappings(m => m
                                     .Map<Prediction>(mp => mp
                                        .Dynamic(dynamic: false)
                                        .AutoMap()
                                        .Properties(ps => ps
                                            .Text(s => s
                                                .Name(n => n.PostDate)
                                                   .Analyzer("standard")))

                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Workclass)
                                                   .IgnoreAbove(50)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Education)
                                                   .IgnoreAbove(40)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.MaritalStatus)
                                                   .IgnoreAbove(30)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Occupation)
                                                   .IgnoreAbove(60)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Relationship)
                                                   .IgnoreAbove(40)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Race)
                                                   .IgnoreAbove(40)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.Sex)
                                                   .IgnoreAbove(10)))
                                        .Properties(ps => ps
                                                   .Keyword(s => s
                                                   .Name(n => n.NativeCountry)
                                                   .IgnoreAbove(100))))));
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }


        // Spremanje podataka u elasticsearch indeks nad kojima će algoritam strojnog učenja izračunati ciljnu varijablu 
        public static IBulkResponse StorePredictions(List<Prediction> predictions)
        {
            Nest.IBulkResponse response = null;
            try
            {
                response = _client.IndexMany<Prediction>(predictions, Constants.ELASTIC_PREDICTION_INDEX_NAME);
                return response;
            }
            catch (Exception ex)
            {
                return response;
            }
        }

        // MatchAll na elasticu za vraćanje unosa svih eksperimenata
        public static List<Prediction> AcquireAllPredicions(int size)
        {
            var result = new List<Prediction>();


            try
            {
                var response = _client.Search<Prediction>(s => s
                       .Index(Constants.ELASTIC_PREDICTION_INDEX_NAME)
                       .From(0)
                       .Size(size)
                       .Query(q => q
                          .MatchAll()));

                if (response != null)
                {
                    var collectionOfRetrievedDocuments = response.Documents;
                    result.AddRange(collectionOfRetrievedDocuments);

                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

    }
}
