﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CsvHelper;
using System.Linq;

namespace Classification.Classes
{
    public class CSVCleaning
    {
        //public static bool cleanCsv(string filePath)
        public static bool cleanCsv(ref string path, string dataType)
        {
            //Console.WriteLine(path);
            Console.WriteLine("ULAZ sa: "+path+"\t"+dataType);
            Console.WriteLine();


            try
            {
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ",";
                    csv.Configuration.HasHeaderRecord = false;
                    var records = csv.GetRecords<CSVClass>().ToList();


                    ChangePath(ref path, dataType);

                    Console.WriteLine("ispis putanje u cleanCsv: " + path);


                    int income = 0;
    
                    using (var writer = new StreamWriter(path))
                    {
                        foreach (CSVClass record in records)
                        {
                            if (record.Workclass.Equals(" ?") || record.Education.Equals(" ?") || record.MaritalStatus.Equals(" ?") || record.Occupation.Equals(" ?") || record.Relationship.Equals(" ?") || record.Race.Equals(" ?") || record.Sex.Equals(" ?") || record.NativeCountry.Equals(" ?"))
                            {
                                continue;
                            }

                            // pretvaranje kategoričke vrijednosti u numeričku
                            if(record.Income.Trim().Equals("<=50K") || record.Income.Trim().Equals("<=50K."))
                            {
                                income = 0;
                            }
                            else if(record.Income.Trim().Equals(">50K") || record.Income.Trim().Equals(">50K."))
                            {
                                income = 1;
                            }

                            writer.Write(record.Age + ", " + record.Workclass.Trim() + ", " + record.Fnlwgt + ", " + record.Education.Trim() + ", " + record.EducationNum + ", " + record.MaritalStatus.Trim() + ", " 
                                         + record.Occupation.Trim() + ", " + record.Relationship.Trim() + ", " + record.Race.Trim() + ", " + record.Sex.Trim() + ", " + record.CapitalGain + ", " 
                                         + record.CapitalLoss + ", " + record.HoursPerWeek + ", " + record.NativeCountry.Trim() + ", " + income + "\r\n");
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }

        public static void ChangePath(ref string path, string dataType)
        {
            path = path.Trim();
            string[] segmenti = path.Split("\\");
            int iteracija = 0;
            path = "";

            foreach (string segment in segmenti)
            {
                iteracija++;
                if (iteracija == segmenti.Length)
                {
                    path += dataType + ".csv";
                }
                else
                {
                    path += segment + "\\";
                }
            }
        }

    }
}
