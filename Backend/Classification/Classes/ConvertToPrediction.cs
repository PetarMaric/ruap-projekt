﻿using System;
using System.Collections.Generic;
using System.Text;
using ElasticsearchSettings.Classes;

namespace Classification.Classes
{
    public class ConvertToPrediction
    {
        public static Prediction MapClassificationToPrediction(ClassificationData classificationData, IncomePrediction incomePrediction)
        {
            return new Prediction
            {
                Age = classificationData.Age,
                Workclass = classificationData.Workclass,
                Fnlwgt = classificationData.Fnlwgt,
                Education = classificationData.Education,
                EducationNum = classificationData.EducationNum,
                MaritalStatus = classificationData.MaritalStatus,
                Occupation = classificationData.Occupation,
                Relationship = classificationData.Relationship,
                Race = classificationData.Race,
                Sex = classificationData.Sex,
                CapitalGain = classificationData.CapitalGain,
                CapitalLoss = classificationData.CapitalLoss,
                HoursPerWeek = classificationData.HoursPerWeek,
                NativeCountry = classificationData.NativeCountry,
                PredictionValue = incomePrediction.Prediction,
                Probability = incomePrediction.Probability,
                PostDate = DateTime.UtcNow
            };
        }
    }
}
