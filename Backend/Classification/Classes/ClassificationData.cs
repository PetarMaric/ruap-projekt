﻿using Microsoft.ML.Runtime.Api;

namespace Classification
{
    public class ClassificationData
    {
        [Column("0")]
        public float Age;

        [Column("1")]
        public string Workclass;

        [Column("2")]
        public float Fnlwgt;

        [Column("3")]
        public string Education;

        [Column("4")]
        public float EducationNum;

        [Column("5")]
        public string MaritalStatus;

        [Column("6")]
        public string Occupation;

        [Column("7")]
        public string Relationship;

        [Column("8")]
        public string Race;

        [Column("9")]
        public string Sex;

        [Column("10")]
        public float CapitalGain;

        [Column("11")]
        public float CapitalLoss;

        [Column("12")]
        public float HoursPerWeek;

        [Column("13")]
        public string NativeCountry;

        [Column("14", name: "Label")]
        public float Income;
    }

    public class IncomePrediction
    {
        // Predicted income
        [ColumnName("PredictedLabel")]
        public bool Prediction { get; set; }

        [ColumnName("Probability")]
        public float Probability { get; set; }

        [ColumnName("Score")]
        public float Score { get; set; }

    }

}
