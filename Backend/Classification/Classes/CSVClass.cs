﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classification.Classes
{
    public class CSVClass
    {
        public float Age { get; set; }

        public string Workclass { get; set; }

        public float Fnlwgt { get; set; }

        public string Education { get; set; }

        public float EducationNum { get; set; }

        public string MaritalStatus { get; set; }

        public string Occupation { get; set; }

        public string Relationship { get; set; }

        public string Race { get; set; }

        public string Sex { get; set; }

        public float CapitalGain { get; set; }

        public float CapitalLoss { get; set; }

        public float HoursPerWeek { get; set; }

        public string NativeCountry { get; set; }

        public string Income { get; set; }
    }

}
