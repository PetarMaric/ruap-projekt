﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Runtime.Api;
using Microsoft.ML.Runtime.Data;


using ElasticsearchSettings;
using ElasticsearchSettings.Classes;
using Classification.Classes;
using System.Threading;
using static Microsoft.ML.Transforms.Normalizers.NormalizingEstimator;
using Microsoft.ML.Runtime;

namespace Classification
{
    public class Program
    {

        // https://docs.microsoft.com/en-us/dotnet/machine-learning/tutorials/sentiment-analysis
        // https://www.codeproject.com/Articles/1249611/%2FArticles%2F1249611%2FMachine-Learning-with-ML-Net-and-Csharp-VB-Net
        // https://docs.microsoft.com/en-us/dotnet/machine-learning/how-to-guides/train-model-categorical-ml-net

        //public static string _trainDataPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\adult.data";
        public static string _trainDataPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\train.csv";

        //public static string _testDataPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\adult.test"
        public static string _testDataPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\test.csv";

        public static string _finalTestDataPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\finalTest.csv";

        public static readonly string _modelPath = "C:\\Users\\petar\\source\\repos\\Ruap projekt\\Backend\\Classification\\Data\\Model.zip";

        //public static MLContext mlContext = new MLContext(seed: 0);

        static TextLoader _textLoader;

        static void Main(string[] args)
        {

            MLContext mlContext = new MLContext(seed: 0);
            _textLoader = mlContext.Data.TextReader(new TextLoader.Arguments
            {
                Separator = ",",
                HasHeader = true,
                Column = new[]
                {
                     new TextLoader.Column("Age", DataKind.R4,0),
                     new TextLoader.Column("Workclass", DataKind.Text,1),
                     new TextLoader.Column("Fnlwgt", DataKind.R4,2),
                     new TextLoader.Column("Education", DataKind.Text,3),
                     new TextLoader.Column("EducationNum", DataKind.R4,4),
                     new TextLoader.Column("MaritalStatus", DataKind.Text,5),
                     new TextLoader.Column("Occupation", DataKind.Text,6),
                     new TextLoader.Column("Relationship", DataKind.Text,7),
                     new TextLoader.Column("Race", DataKind.Text,8),
                     new TextLoader.Column("Sex", DataKind.Text,9),
                     new TextLoader.Column("CapitalGain", DataKind.R4,10),
                     new TextLoader.Column("CapitalLoss", DataKind.R4,11),
                     new TextLoader.Column("HoursPerWeek", DataKind.R4,12),
                     new TextLoader.Column("NativeCountry", DataKind.Text,13),
                     new TextLoader.Column("Label", DataKind.Bool,14)
                 }
            });


            IDictionary<int, string> DataType = new Dictionary<int, string>()
                                                                         {
                                                                             { 0, "train" },
                                                                             { 1, "test" }
                                                                         };
            //CSVCleaning.cleanCsv(ref _trainDataPath, DataType[0]);

            //CSVCleaning.cleanCsv(ref _testDataPath, DataType[1]);


            //var model = Train(mlContext, _trainDataPath);

            //Evaluate(mlContext, model);

            //SaveModelAsFile(mlContext, model);

            //FinalEvaluation(_finalTestDataPath);





        }

        public static ITransformer Train(MLContext mlContext, string dataPath)
        {
            IDataView dataView = _textLoader.Read(dataPath);

            var dynamicPipeline = mlContext.Transforms.Concatenate("CategoricalFeatures", "Workclass", "Education", "MaritalStatus", "Occupation", "Relationship", "Race", "Sex", "NativeCountry")
                                           //.Append(mlContext.Transforms.Normalize("Age", mode: NormalizerMode.MeanVariance ))
                                           //.Append(mlContext.Transforms.Normalize("Fnlwgt" ,mode: NormalizerMode.MinMax))
                                           //.Append(mlContext.Transforms.Normalize("EducationNum", mode: NormalizerMode.MinMax))
                                           //.Append(mlContext.Transforms.Normalize("HoursPerWeek", mode: NormalizerMode.Binning))
                                           .Append(mlContext.Transforms.Categorical.OneHotEncoding("CategoricalFeatures", "CategoricalBag", outputKind: Microsoft.ML.Transforms.Categorical.OneHotEncodingTransformer.OutputKind.Bag))
                                           .Append(mlContext.Transforms.Concatenate("NumericalFeatures", "Age", "Fnlwgt", "EducationNum", "CapitalGain", "CapitalLoss", "HoursPerWeek"));
            //.Append(mlContext.Transforms.Normalize("NumericalFeatures", mode: NormalizerMode.MinMax));
            //.Append(mlContext.Transforms.Normalize("CategoricalBag", mode: NormalizerMode.MinMax ));

            var fullLearningPipeline = dynamicPipeline.Append(mlContext.Transforms.Concatenate("Features", "NumericalFeatures", "CategoricalBag"))
                                                       .Append(mlContext.BinaryClassification.Trainers.FastTree(numLeaves: 17, numTrees: 100, minDatapointsInLeaves:10, learningRate: 0.12));
                                                       //.Append(mlContext.BinaryClassification.Trainers.AveragedPerceptron(learningRate: (float)0.0001, decreaseLearningRate: false, l2RegularizerWeight: (float)0, numIterations: 200 ));
                                                       //.Append(mlContext.BinaryClassification.Trainers.FastForest(numLeaves: 50, numTrees: 200, minDatapointsInLeaves:16, learningRate:0.113));
                                                       //.Append(mlContext.BinaryClassification.Trainers.StochasticGradientDescent(maxIterations:100, initLearningRate: (float)0.01, l2Weight: (float)1E-12));


            Console.WriteLine("=============== Create and Train the Model ===============");
            var model = fullLearningPipeline.Fit(dataView);
            Console.WriteLine("=============== End of training ===============");
            Console.WriteLine();

            return model;

        }

        public static void Evaluate(MLContext mlContext, ITransformer model)
        {
            IDataView dataView = _textLoader.Read(_testDataPath);
            Console.WriteLine("=============== Evaluating Model accuracy with Test data ===============");
            var predictions = model.Transform(dataView);

            var metrics = mlContext.BinaryClassification.Evaluate(predictions, "Label");
            //var metrics = mlContext.BinaryClassification.EvaluateNonCalibrated(predictions, "Label");

            Console.WriteLine();
            Console.WriteLine("Model quality metrics evaluation");
            Console.WriteLine("--------------------------------");
            Console.WriteLine($"Accuracy: {metrics.Accuracy:P2}");
            Console.WriteLine($"Positive precision: {metrics.PositivePrecision:P2}");
            Console.WriteLine($"Positive recall: {metrics.PositiveRecall:P2}");
            Console.WriteLine($"Negative precision: { metrics.NegativePrecision:P2}");
            Console.WriteLine($"Negative recall: { metrics.NegativeRecall:P2}");
            Console.WriteLine($"Area under precision curve: { metrics.Auprc}");
            Console.WriteLine($"F1Score: {metrics.F1Score:P2}");
            Console.WriteLine("=============== End of model evaluation ===============");

        }

        private static void SaveModelAsFile(MLContext mlContext, ITransformer model)
        {
            using (var fs = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
                mlContext.Model.Save(model, fs);
            Console.WriteLine("The model is saved to {0}", _modelPath);
        }

        private static void Predict(MLContext mlContext, ITransformer model)
        {
            var predictionFunction = model.MakePredictionFunction<ClassificationData, IncomePrediction>(mlContext);

            ClassificationData primjerPodatakaZaPredikciju = new ClassificationData
            {
                Age = 50,
                Workclass = "Private",
                Fnlwgt = 180000,
                Education = "Doctorate",
                EducationNum = 15,
                MaritalStatus = "Married-civ-spouse",
                Occupation = "Prof-specialty",
                Relationship = "Wife",
                Sex = "Male",
                CapitalGain = 3235,
                CapitalLoss = 0,
                HoursPerWeek = 40,
                NativeCountry = "Yugoslavia"
            };

            var resultPrediction = predictionFunction.Predict(primjerPodatakaZaPredikciju);

            Console.WriteLine();
            Console.WriteLine("=============== Prediction Test of model with a single sample and test dataset ===============");

            Console.WriteLine();
            Console.WriteLine($"Prediction: {(Convert.ToBoolean(resultPrediction.Prediction) ? ">50K" : "<=50K")} | Probability: {resultPrediction.Probability} ");
            Console.WriteLine("=============== End of Predictions ===============");
            Console.WriteLine();
        }

        public static void PredictWithModelLoadedFromFile(MLContext mlContext)
        {
            ITransformer loadedModel;

            IEnumerable<ClassificationData> primjerPodatakaZaPredikciju = new[]
            {

                    new ClassificationData
                    {
                        Age = 50,
                        Workclass = "Private",
                        Fnlwgt = 180000,
                        Education = "Doctorate",
                        EducationNum = 15,
                        MaritalStatus = "Married-civ-spouse",
                        Occupation = "Prof-specialty",
                        Relationship = "Wife",
                        Sex = "Male",
                        CapitalGain = 3235,
                        CapitalLoss = 0,
                        HoursPerWeek = 40,
                        NativeCountry = "Yugoslavia"
                    },
                    new ClassificationData
                    {
                        Age = 34,
                        Workclass = "Private",
                        Fnlwgt = 357342,
                        Education = "Doctorate",
                        EducationNum = 15,
                        MaritalStatus = "Married-civ-spouse",
                        Occupation = "Prof-specialty",
                        Relationship = "Wife",
                        Sex = "Male",
                        CapitalGain = 0,
                        CapitalLoss = 0,
                        HoursPerWeek = 40,
                        NativeCountry = "United-States"
                    }

             };

            using (var fileStream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = mlContext.Model.Load(fileStream);

                var classificationDataStream = mlContext.CreateStreamingDataView(primjerPodatakaZaPredikciju);
                var predictions = loadedModel.Transform(classificationDataStream);

                var predictedResults = predictions.AsEnumerable<IncomePrediction>(mlContext, reuseRowObject: false);

                Console.WriteLine();

                Console.WriteLine("=============== Prediction Test of loaded model with a multiple samples ===============");

                var primjerPodatakaIPredikcije = primjerPodatakaZaPredikciju.Zip(predictedResults, (primjerPodataka, predikcija) => (primjerPodataka, predikcija));

                foreach (var item in primjerPodatakaIPredikcije)
                {
                    Console.WriteLine($"Native country: {item.primjerPodataka.NativeCountry} | Prediction: {(Convert.ToBoolean(item.predikcija.Prediction) ? ">50K" : "<=50K")} | Probability: {item.predikcija.Probability} ");
                }
                Console.WriteLine("=============== End of predictions ===============");
            }

        }




        public static Prediction MakePrediction(ClassificationData experimentData)
        {
            ITransformer loadedModel;
            MLContext mlContext = new MLContext(seed: 0);

            using (var fileStream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = mlContext.Model.Load(fileStream);

                var predictionFunction = loadedModel.MakePredictionFunction<ClassificationData, IncomePrediction>(mlContext);

                var resultPrediction = predictionFunction.Predict(experimentData);

                return ConvertToPrediction.MapClassificationToPrediction(experimentData, resultPrediction);

            }

        }

        public static void FinalEvaluation(string path)
        {
            ITransformer loadedModel;
            IDataView dataView = _textLoader.Read(path);
            Console.WriteLine("=============== Final evaluation ===============");
            MLContext mlContext = new MLContext(seed: 0);

            using (var fileStream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = mlContext.Model.Load(fileStream);

                var predictions = loadedModel.Transform(dataView);
                var metrics = mlContext.BinaryClassification.Evaluate(predictions, "Label");

                Console.WriteLine();
                Console.WriteLine("Model quality metrics of final evaluation");
                Console.WriteLine("--------------------------------");
                Console.WriteLine($"Accuracy: {metrics.Accuracy:P2}");
                Console.WriteLine($"Positive precision: {metrics.PositivePrecision:P2}");
                Console.WriteLine($"Positive recall: {metrics.PositiveRecall:P2}");
                Console.WriteLine($"Negative precision: { metrics.NegativePrecision:P2}");
                Console.WriteLine($"Negative recall: { metrics.NegativeRecall:P2}");
                Console.WriteLine($"Area under precision curve: { metrics.Auprc}");
                Console.WriteLine($"F1Score: {metrics.F1Score:P2}");
                Console.WriteLine("=============== End of final model evaluation ===============");
            }
        }


    }
}

