
Ovaj projekt izrađen je koristeći razvojno okruženje Visual Studio za izradu backend dijela ovog projekta i Visual Studio Code pomoću Angular okvira za kreiranje frontend dijela ovog projekta.
Programski jezici koji su bili korišteni su C# i Typescript. Također je korišten i označni jezik HTML za kreiranje formi.
Sastavni dijelovi backend dijela projekta:
		<pre>
		1. Registracija lokacije podatkovnih skupova za treniranje i testiranje modela strojnog učenja
		2. Izrada cjevovoda kojim se učitavaju i transformiraju podaci kako bi se omogućilo kreiranje modela
		3. Izrada modela i njegova pohrana na disk kako bi se omogućila njegova daljnja uporaba
		4. Pružanje potpore pohrani "pokusnih" podataka u indeks Elasticsearcha
		5. Pružanje potpore pretrazi sadržaja u indeksu Elasticsearcha kako bi se vidjeli pohranjeni eksperimenti
		6. Web servis s dva endpointa kojeg koristi frontend dio: a) api/predict - "odaziva" se na HTTP POST zahtjev s JSON objektom u body elementu zahtjeva. JSON objekt mora biti tipa ClassificationData te se automatski deserijalizira u objekt klase ClassificationData za daljnju upotrebu
																  b) api/matchall - "odaziva" se na HTTP GET zahtjev; querija Elasticsearch indeks s pohranjenim eksperimentima; mogući parametar upita je size kojim se može specificirati broj rezultata kojeg će vratiti web servis u JSON formatu 
		</pre>
Sastavni dijelovi frontend dijela projekta: 
		<pre>
		1. Komponenta za registraciju:
				Omogućava korisniku registraciju kako bi mogao koristiti funkcionalnost frontenda. Uputno je koristiti web servis poput mailinatora za kreiranje korisničkog računa za testiranje rada aplikacije.
				Nakon ispravnog popunjavanja forme, Baasic tvrtke MONO šalje mail formatiran unutar web aplikacije koji sadržava formu za aktivaciju računa te preusmjeravanje na stranicu prijave. Nakon toga Vaš
				račun je upisan kao validan u modul u Baasicu te vam se za vrijeme sesije dodjeljuje token pohranjen u local storageu web preglednika. 
		2. Komponenta ta prijavu:
				Omogućava prijavu prethodno registriranog korisnika. Omogućava daljnji rad u aplikaciji pošto aplikacija koristi kreirani Guard servis koji koristi token pohranjen u local storageu kao značajku da je
				korisnik prijavljen. Token se briše na logout radnju te gubi pravo pristupa komponenti predikcije.
		3. Komponenta za oporavak od zaboravljene lozinke
		4. Komponenta za aktivaciju korisničkog računa
		5. Komponenta predikcije: 
				Glavni dio ovog dijela projekta. Omogućava korisniku odabir i unos određenih značajki koje se žele uputiti na backend dio kako bi se izvršilo učenje. Kao rezultat backend dio šalje objekt tipa ExperimentData
				koji je implementiran u frontend dijelu rada. Također se na dolazak tog JSON objekta otvara pop-up prozor u kojem se prikazuju izračunati parametri.
		</pre>
<br>
<hr>
<br>
Preduvjeti za pokretanje projekta su:
		<pre>
		1. U backend dijelu projekta instalirani NuGet paketi:
				-u Classification segmentu:
					a) <a href="https://www.nuget.org/packages/CsvHelper/12.1.0">CsvHelper 12.1.0</a>
					b) <a href="https://www.nuget.org/packages/Microsoft.ML/0.8.0">Microsoft.ML 0.8.0</a>
				-u ElasticsearchSettings segmentu:
					a) <a href="https://www.nuget.org/packages/NEST/6.4.1">NEST 6.4.1</a>
				-u RestApi segmentu:
					a) <a href="https://dotnet.microsoft.com/download/dotnet-core/2.2">.NET Core 2.2.0</a>
		2. Pokrenut <a href="https://www.elastic.co/downloads/past-releases/elasticsearch-6-5-4">Elasticsearch 6.5.4</a> pokrenut na portu 9200 što bi trebala biti defaultna vrijednost.
			a) Za pokretanje Elasticsearch servisa potrebno je instalirati <a href="https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html">Javu SDK 11.0.2</a> te dodati putanju javinog bin direktorija u 'Path' varijablu okruženja Windows sustava. Zatim treba pokrenuti Elasticsearch odlaskom u mapu elasticsearch-6.5.4/bin/elasticsearch.bat. Ako se zatvori prozor Elasticseracha nakon dvostrukog klika, potrebno je provjeriti ispravnost putanje do Java SDK u varijablama okruženja.
		3. Pokrenut RestApi u backend dijelu projekta koristeći IIS Express konfiguraciju. API bi trebao biti dostupan na portu 60396.
		4. U frontend dijelu projekta: 
				-<a href="https://nodejs.org/en/download/">Node.js 10.15.1</a>
				-pokrenut 'npm install' u root mapi frontend dijela projekta
		</pre>